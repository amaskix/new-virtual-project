﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {


    public GameObject player;
    private Vector3 offset;
   // private float rotationX = 0f;
   // private float rotationZ = 0f;
    public float turnSpeed = 30f;
    public float yaw = 0;
    public float pitch = 0;

    // Use this for initialization
    void Start()
    {
        offset = transform.position - player.transform.position;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //transform.position = player.transform.position + offset;
        //Quaternion rotation = Quaternion.Euler(-rotationX, yaw, -rotationZ);
        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offset;
        transform.position = player.transform.position +  offset;
        //transform.Rotate(-rotationX, yaw, -rotationZ);
        transform.LookAt(player.transform);
    }

    void Update()
    {
       /* rotationX = 30 * Time.deltaTime * Input.GetAxis("Mouse Y");
        yaw = 30 * Time.deltaTime * Input.GetAxis("Mouse X");
        rotationZ = 5 * 30 * Time.deltaTime * Input.GetAxis("Horizontal");*/
    }

}

