﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToBattle : MonoBehaviour {
    public Transform CenterOfBattle;
    public float RadiusOfGameBattle = 500;
    public GameObject ReturnBackText;
    public List<GameObject> TextTimer = new List<GameObject>();
    private float Timer = 0;
    private int Index = 0;
    private void Update()
    {
        
       if (Vector3.Distance(CenterOfBattle.position, transform.position)>RadiusOfGameBattle)
        {
            Debug.Log("return to battle");
            TimerWork();
        }
        else
        {
            ReturnBackText.SetActive(false);
            TextTimer[Index].SetActive(false);
            TextTimer[0].SetActive(true);
            Index = 0;
            Debug.Log(Vector3.Distance(CenterOfBattle.position, transform.position));
        }
    }
    private void TimerWork()
    {
        ReturnBackText.SetActive(true);
        Timer++;
        if (Timer > 60)
        {
            Timer = 0;
            Index++;
            TextTimer[Index - 1].SetActive(false);
            if (Index <10)
            {
                TextTimer[Index].SetActive(true);
            }
            else
            {
                
            }

        }
    }
}
