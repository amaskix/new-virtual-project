﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControllerScript : MonoBehaviour {
    public float movementSpeed = 20;
    private float counter = 100;
    private float movmentCounter = 100;
    public Transform target;
    public float impactRadius = 1.5f;
    private Rigidbody rb;
    private bool rotationIsDone = false;
    public GameObject Sphere;
    public Transform bulletSpawn;
    public GameObject bulletPrefap;
    private int sphereCounter = 0;
    public int HP = 100;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update () {
        if (sphereCounter == 0)
        {
            Sphere.SetActive(false);
        }
        else
        {
            sphereCounter--;
        }
        float distance = Vector3.Distance(target.position, transform.position);
               if (distance < impactRadius&&counter == 0)
        {
            counter = 100;
            rb.AddForce(transform.forward * movementSpeed, ForceMode.Acceleration);
        }
                   
        if (counter!=0)
        {
            counter--;
            FaceTarget();
            movmentCounter = distance;
        }
       
        if (counter == 0&&movmentCounter>0)
       {
            
            movmentCounter--;
            rb.AddForce(transform.forward * movementSpeed, ForceMode.Acceleration);
            
            if (movmentCounter<1&&movmentCounter>-1)
            {
                counter = 100;
            }
        }
        rotationIsDone = false;
       
        
        rb.velocity *= 0.98f;
        rb.angularVelocity *= 0.98f;

    }
    void FaceTarget()
    {
       
        Vector3 direction = (target.position - transform.position).normalized;//geting a movement vector
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, direction.y, direction.z));//geting a rotation angle
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime*3f);//rotating towards target
      
       
    }
    void Fire()
    {
        var bullet = (GameObject)Instantiate(
           bulletPrefap,
           bulletSpawn.position,
           bulletSpawn.rotation);
    }
    void OnCollisionEnter(Collision col)
    {
        HP --;
        if (HP< 0)
        {
            Destroy(this.gameObject);
        }
        Sphere.SetActive(true);
        sphereCounter = 60;
        if (col.gameObject.CompareTag("Asteroid"))
        {
            if (col.gameObject.transform.lossyScale.x < 10)
                col.gameObject.SetActive(false);
        }
        //velocity -= velocity;
    }
}
