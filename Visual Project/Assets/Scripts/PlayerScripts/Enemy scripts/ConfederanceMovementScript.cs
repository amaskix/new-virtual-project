﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfederanceMovementScript : MonoBehaviour {

    public float movementSpeed = 100;
    private float counter = 0;
    public Transform target;
    public float impactRadius = 0;
    private void Start()
    {
        
    }
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance > impactRadius && counter == 0)
        {
            FaceTarget();
        }
        else
        {
            if (counter < 120)
            {
                counter++;
            }
            else
            {
                counter = 0;
            }
        }
        transform.position += transform.forward * movementSpeed * Time.deltaTime;

    }
    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;//geting a movement vector
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, direction.y, direction.z));//geting a rotation angle
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);//rotating towards target
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Asteroid"))
        {
            other.gameObject.SetActive(false);
        }
    }
}
