﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementScript : MonoBehaviour {
    public float speed = 20;
    public float RotationSpeed = 5;
    public Transform target;
    public float rayCastOffset = 2.5f;
    public float detactionDistance = 20;
    public Transform bulletSpawn;
    public GameObject bulletPrefap;
    private float VolleyCounter = 0;
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        Vector3 rotation = transform.position;
        Debug.DrawRay(rotation, transform.forward * detactionDistance, Color.blue);
        if (Vector3.Distance(target.position, transform.position) > detactionDistance-2)
        { 
            Move();
            PathFinding();
        }
        else
        {
            VolleyCounter += 1 * Time.deltaTime;
            TurnToTarget();
            if (VolleyCounter >1)
            {               
                SpawnBullet();
                VolleyCounter = 0;
            }
        }                      
       
	}
    void Turn()
    {
        Vector3 direction = (target.position - transform.position).normalized;//geting a movement vector
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, direction.y, direction.z));//geting a rotation angle
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 3f);//rotating towards target
    }
    private void Move()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }
    void TurnToTarget()
    {
        RaycastHit hit;
        Vector3 raycastOffset = Vector3.zero;
        Vector3 left = transform.position - transform.right * rayCastOffset;
        Vector3 right = transform.position + transform.right * rayCastOffset;
        Vector3 up = transform.position + transform.up * rayCastOffset;
        Vector3 down = transform.position - transform.up * rayCastOffset;
        Debug.DrawRay(left, transform.forward * detactionDistance, Color.blue);
        Debug.DrawRay(right, transform.forward * detactionDistance, Color.blue);
        Debug.DrawRay(up, transform.forward * detactionDistance, Color.blue);
        Debug.DrawRay(down, transform.forward * detactionDistance, Color.blue);
        if (Physics.Raycast(left, transform.forward, out hit, detactionDistance))
            raycastOffset -= Vector3.right;
        else if (Physics.Raycast(right, transform.forward, out hit, detactionDistance))
            raycastOffset += Vector3.right;

        if (Physics.Raycast(up, transform.forward, out hit, detactionDistance))
            raycastOffset += Vector3.up;
        else if (Physics.Raycast(down, transform.forward, out hit, detactionDistance))
            raycastOffset -= Vector3.up;
        
        if (raycastOffset != Vector3.zero)
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(raycastOffset), 1f * Time.deltaTime);
        Debug.Log("Rotates Towards Target");
    }

    void PathFinding()
    {
        RaycastHit hit;
        Vector3 raycastOffset = Vector3.zero;
        Vector3 left = transform.position - transform.right * rayCastOffset;
        Vector3 right = transform.position + transform.right * rayCastOffset;
        Vector3 up = transform.position + transform.up * rayCastOffset;
        Vector3 down = transform.position - transform.up * rayCastOffset;
        Debug.DrawRay(left, transform.forward*detactionDistance, Color.blue);
        Debug.DrawRay(right, transform.forward * detactionDistance, Color.blue);
        Debug.DrawRay(up, transform.forward * detactionDistance, Color.blue);
        Debug.DrawRay(down, transform.forward * detactionDistance, Color.blue);

        if (Physics.Raycast(left,transform.forward, out hit, detactionDistance))        
            raycastOffset += Vector3.right;        
        else if (Physics.Raycast(right, transform.forward, out hit, detactionDistance))        
            raycastOffset -= Vector3.right;           
        
        if (Physics.Raycast(up, transform.forward, out hit, detactionDistance))        
            raycastOffset -= Vector3.up;        
        else if (Physics.Raycast(down, transform.forward, out hit, detactionDistance))        
            raycastOffset += Vector3.up;

        if (raycastOffset != Vector3.zero)        
            transform.Rotate(raycastOffset * 5f * Time.deltaTime);        
        else        
            Turn();
        
    }
    void volley()
    {
        
    }
    void SpawnBullet()
    {
        var bullet = (GameObject)Instantiate(
            bulletPrefap,
            bulletSpawn.position,
            bulletSpawn.rotation);
    }
}
