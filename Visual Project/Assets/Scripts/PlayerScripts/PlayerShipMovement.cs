﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerShipMovement : MonoBehaviour {
    public float movementSpeed = 100;
    public float turnSpeed = 30f;
    public float maximumSpeed = 200;
    public float maxboostSpeed = 300;
    public Transform bulletSpawn;
    public float HP = 200;
    public GameObject bulletPrefap;

    private Rigidbody rb;
    public GameObject Sphere;
    private int sphereCounter = 0;

    private int spawnTimeCounter=5;

	void Start () {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
    }


    void LateUpdate()
    {
        Vector3 cameraPos = transform.position - transform.forward * 3f + transform.right * 2f + Vector3.up * 2f;
        //float bias = 0.666f;
        Camera.main.transform.position =
           cameraPos;
        Camera.main.transform.LookAt(transform.position + transform.forward * 15f + Vector3.up * 5f);
    }



    void Update () {
        SpeedBoost();
        if (Input.GetAxis("Vertical") > 0)
        {
            if (rb.velocity.magnitude < maximumSpeed)
            {

                rb.AddForce(transform.forward * movementSpeed, ForceMode.Acceleration);
            }
        }

        if (Input.GetAxis("Vertical") < 0)
        {
            rb.velocity *= 0.98f;
            rb.angularVelocity *= 0.98f;
            
        }
        if(Input.GetKeyDown(KeyCode.E))
        {
            rb.AddTorque(transform.forward * 10 * -1f, ForceMode.Impulse);
            rb.AddForce(transform.right * 10000);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            rb.AddTorque(transform.forward * 10 * 1f, ForceMode.Impulse);
            rb.AddForce(transform.right * 10000 * -1f);
        }
        Turn();
        rb.velocity *= 0.98f;
        rb.angularVelocity *= 0.98f;

        
        if (Input.GetKey(KeyCode.Space))
        {
            spawnTimeCounter--;

            if (spawnTimeCounter == 0)
            {
                spawnTimeCounter = 5;
                SpawnBullet();
            }
        }

        if (sphereCounter == 0)
        {
            Sphere.SetActive(false);
        }
        else
        {
            sphereCounter--;
        }
        
        
    }
    void Turn()
    {
        float rotationX = turnSpeed * Time.deltaTime * Input.GetAxis("Mouse Y");
        float yaw = turnSpeed * Time.deltaTime * Input.GetAxis("Mouse X");
        float rotationZ = 5*turnSpeed * Time.deltaTime * Input.GetAxis("Horizontal");
        transform.Rotate(-rotationX, yaw, -rotationZ);
        //Camera.main.transform.Rotate(0, 0, -rotationZ);
    }

    private void SpeedBoost()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (rb.velocity.magnitude < maxboostSpeed)
            {
                //velocity -= transform.forward * movementSpeed * Time.deltaTime * Input.GetAxis("Vertical");
                rb.AddForce(transform.forward * movementSpeed, ForceMode.Acceleration);
                //rb.AddForce(Camera.main.transform.forward.normalized * 20);

            }
        }
        else
        {
            if (rb.velocity.magnitude > maximumSpeed)
            {

                rb.velocity *= 0.98f;
                rb.angularVelocity *= 0.98f;
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        HP--;
        if (HP<0)
        {
            EndOfAGame();
        }
        Sphere.SetActive(true);
        sphereCounter = 60;
        if (col.gameObject.CompareTag("Asteroid"))
        {
            if (col.gameObject.transform.lossyScale.x < 10)
                col.gameObject.SetActive(false);
        }
        //velocity -= velocity;
    }

    void SpawnBullet()
    {
        var bullet = (GameObject)Instantiate(
            bulletPrefap,
            bulletSpawn.position,
            bulletSpawn.rotation);
    }
    public void EndOfAGame()
    {
        Destroy(this.gameObject);
    }
}
