﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {

    public GameObject bullet;
    

    private float CurrentLaunchForce;        

    private float ChargeSpeed;
    private float ChargeTime = 0.75f;


    // Use this for initialization
    void Start () {

        CurrentLaunchForce = 150;
    }
	
	// Update is called once per frame

	void Update () {      
           Fire();        

    }

    void Fire()
    { 
        bullet.transform.position -= bullet.transform.forward *CurrentLaunchForce* Time.deltaTime;


        Destroy(bullet, 8);

    }

    void OnCollisionEnter(Collision other)
    {
        Destroy(this.gameObject);
        if (other.gameObject.CompareTag("EnemyShip"))
        {
            Destroy(this.gameObject);
        }
        if (other.gameObject.CompareTag("Asteroid"))

        {
            Destroy(this.gameObject);
        }
        //velocity -= velocity;
    }
    
}
